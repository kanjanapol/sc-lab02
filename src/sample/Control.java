package sample;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;



public class Control {
	class AddInterestListener implements ActionListener
    {
       public void actionPerformed(ActionEvent event)
       
       {
    	   double rate = frame.getinterest();
           double interest = bank.getBalance() * rate / 100;
           bank.deposit(interest);
           frame.setResult("balance: " + bank.getBalance());
    	  
    	  
          
       }            
    }
	
	BankAccount bank;
	ActionListener list;
	InvestmentFrame frame;
	
	
	public Control(){
		   bank = new BankAccount();
		   frame = new InvestmentFrame();
		   	frame.pack();
			frame.setVisible(true);
			frame.setSize(600, 400);
			list = new AddInterestListener();
			frame.setListener(list);
			
			
		}

	
	
	public Control(double initialBalance) {
		// TODO Auto-generated constructor stub
		bank = new BankAccount( initialBalance);
	}
	public void deposite(double amount){
		bank.deposit(amount);
	}
	public void withdraw(double amount){
		bank.withdraw(amount);
	}
	
	public double getBalance(){
		 return  bank.getBalance();
	}
	
	
	
}
